package Ejecutar;

import Modelo.*;
import java.awt.Color;
import java.util.ArrayList;

public class JuegoNave {

    public static int aleatorio(int max, int min) {

        return (int) (Math.random() * (max - min));
    }

    public static void main(String[] args) {

        Ventana newWindow = new Ventana("Juego");
        ArrayList arregloDeObjetos = new ArrayList();

        Coordenada cor1 = new Coordenada(250, 250);
        Coordenada cor2 = new Coordenada(350, 350);

        //nave
        Coordenada corN1 = new Coordenada(475, 450);
        Coordenada corN2 = new Coordenada(425, 500);
        Coordenada corN3 = new Coordenada(525, 500);

        // RectanguloGrafico rectangulo = new RectanguloGrafico(cor1,50,30,Color.BLACK);
        //CirculoGrafico circulo = new CirculoGrafico(cor2,50,Color.red);
        TrianguloGrafico triangulo = new TrianguloGrafico(corN1, corN2, corN3, Color.GREEN);

        //arregloDeObjetos.add(rectangulo);
        //arregloDeObjetos.add(circulo);
        arregloDeObjetos.add(triangulo);

        Panel nuestroPanel = new Panel(arregloDeObjetos);

        int rango1 = aleatorio(800, 50);
        Coordenada salida1 = new Coordenada(rango1, -25);
        RectanguloGrafico asteroide1 = new RectanguloGrafico(salida1, 25, 25, Color.GRAY);
        int rango2 = aleatorio(800, 50);
        Coordenada salida2 = new Coordenada(rango2, -25);
        RectanguloGrafico asteroide2 = new RectanguloGrafico(salida2, 25, 25, Color.GRAY);
        int rango3 = aleatorio(800, 50);
        Coordenada salida3 = new Coordenada(rango3, -25);
        RectanguloGrafico asteroide3 = new RectanguloGrafico(salida3, 25, 25, Color.GRAY);
        int rango4 = aleatorio(800, 50);
        Coordenada salida4 = new Coordenada(rango4, -25);
        RectanguloGrafico asteroide4 = new RectanguloGrafico(salida4, 25, 25, Color.GRAY);
        int rango5 = aleatorio(800, 50);
        Coordenada salida5 = new Coordenada(rango5, -25);
        RectanguloGrafico asteroide5 = new RectanguloGrafico(salida5, 25, 25, Color.GRAY);
        arregloDeObjetos.add(asteroide1);
        arregloDeObjetos.add(asteroide2);
        arregloDeObjetos.add(asteroide3);
        arregloDeObjetos.add(asteroide4);
        arregloDeObjetos.add(asteroide5);

        nuestroPanel.refNave(triangulo);
        nuestroPanel.refAst(asteroide1, asteroide2, asteroide3, asteroide4, asteroide5);
       

        TextoGrafico vidas = new TextoGrafico("Vidas", Color.BLACK, 1700, 50);
        vidas.setSize(35);
        arregloDeObjetos.add(vidas);

        TextoGrafico score = new TextoGrafico("Puntos", Color.BLACK, 1700, 250);
        score.setSize(35);
        arregloDeObjetos.add(score);

        TextoGrafico numero = new TextoGrafico("0", Color.RED, 1700, 350);
        numero.setSize(40);
        arregloDeObjetos.add(numero);

        TextoGrafico vidasNum = new TextoGrafico("3", Color.RED, 1700, 150);
        vidasNum.setSize(40);
        arregloDeObjetos.add(vidasNum);

        TextoGrafico textoFinal = new TextoGrafico("FINAL :V", Color.RED, 900, 500);
        textoFinal.setSize(100);
        
         nuestroPanel.refPuntos(numero);
         nuestroPanel.refFinal(textoFinal);
         nuestroPanel.refVidas(vidasNum);

        newWindow.add(nuestroPanel);
        newWindow.setSize(1000, 600);
        newWindow.setVisible(true);
        nuestroPanel.iniciar();
    }

}
