
package Modelo;


public class Coordenada {

private float x;
private float y;


public Coordenada(){
    
}

public Coordenada(float x, float y){
    this.x = x;
    this.y = y;
}

public Coordenada(Coordenada nuevaC){
    this.x = nuevaC.x;
    this.y = nuevaC.y;
}

public float getX(){
    return this.x;
}
public void setX(float x){
   this.x = x;
}
public float geyY(){
    return this.y;
}
public void setY(float y){
    this.y = y;
}

public Coordenada suma(Coordenada S){
    float Sumx = this.x = x + S.getX();
    float Sumy = this.y = y + S.geyY();
    
    Coordenada Cor = new Coordenada(Sumx, Sumy);
    
    return Cor;
}
    
}
