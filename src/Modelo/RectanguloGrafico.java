
package Modelo;

import java.awt.Color;
import java.awt.Graphics;

public class RectanguloGrafico extends Rectangulo implements Dibujable {
    
    Color color;
    
    public RectanguloGrafico(Coordenada coor, float x, float y, Color uncolor){
        super(coor,x,y);
        this.color = uncolor;
    }

    @Override
    public void dibujar(Graphics ag) {
    ag.setColor(color);
    ag.fillRect((int)this.getX(),(int)this.geyY(), (int)this.getLado(1), (int)this.getLado(2));
            
    }
    public void ciclo(int mov){
        float x = this.geyY();
        this.setY(x += mov );
    }
    
    public void pintar(Color uncolor){
        this.color = uncolor;
    }
    
}
