
package Modelo;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

public class TrianguloGrafico extends   Triangulo implements Dibujable {

    Color color;
    
    
    public TrianguloGrafico(Coordenada a,Coordenada b, Coordenada c, Color uncolor){
        super(a,b,c);
        this.color = uncolor;
    }
    
    
    @Override
    public void dibujar(Graphics ag) {
        
        ag.setColor(color);
    
        int x[] = {(int)this.getX(),(int)this.cor1.getX(),(int)this.cor2.getX()};
        int y[] = {(int)this.geyY(),(int)this.cor1.geyY(), (int)this.cor2.geyY()};
        
        Polygon p = new Polygon(x,y,3);
        
        ag.fillPolygon(p);
        
        
    }
    public void pintar(Graphics ag, Color uncolor) {
        
        ag.setColor(uncolor);
    
        int x[] = {(int)this.getX(),(int)this.cor1.getX(),(int)this.cor2.getX()};
        int y[] = {(int)this.geyY(),(int)this.cor1.geyY(), (int)this.cor2.geyY()};
        
        Polygon p = new Polygon(x,y,3);
        
        ag.fillPolygon(p);
        
        
    }
    
    public CirculoGrafico bala(){
        Coordenada salida = new Coordenada(this.getX(),this.geyY());
        CirculoGrafico bala = new CirculoGrafico(salida,10,Color.RED);
        return bala;
    }
    
    public void ciclo(){
        for (int i = 0; i < this.balas.size(); i++) {
            CirculoGrafico y =  ( CirculoGrafico)this.balas.get(i);
            float x = y.geyY();
            y.setY(x -= 5);
        }
    }
    
}
