package Modelo;

import static Ejecutar.JuegoNave.aleatorio;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;

public class Panel extends JPanel implements KeyListener {
    
    ArrayList v;
    ArrayList ast = new ArrayList();
    
    TrianguloGrafico trinagulo;
    
    boolean FinJuego =true;
    
    Coordenada movimientoIzq = new Coordenada(-25,0);
    Coordenada movimientoDer = new Coordenada(25,0);
    Coordenada movimientoNulo = new Coordenada(0,0);
    
    TextoGrafico puntos;
    TextoGrafico vidas;
    TextoGrafico finaltr;
    
    
    
   int contadorAsteroides =5;
   int score;
   int numVidas =3;
   int MOV =10;
   int MAXAST =5;
   

    public Panel(ArrayList vss){
        this.v = vss;
        this.addKeyListener(this);
        setFocusable(true);
    }
    
    public void paint(Graphics g){
        
        Dimension d = getSize();
        Image Imagen= createImage(d.width,d.height);
        Graphics buff = Imagen.getGraphics();
        
        Dibujable dib;
        for (int i = 0; i < v.size(); i++) {
         dib = (Dibujable)v.get(i);
         dib.dibujar(buff);
        }
        g.drawImage(Imagen, 0, 0, null);
    }
    
    @Override
    public void update(Graphics g){
        paint(g);
    }

    @Override
    public void keyTyped(KeyEvent ke) {
 
    }

    @Override
    public void keyPressed(KeyEvent ke) {
    
        int tecla = ke.getKeyCode();
        
        if(tecla == KeyEvent.VK_LEFT){
    
            this.trinagulo.mover(movimientoIzq);
        
        }
         if(tecla == KeyEvent.VK_RIGHT){
            
              this.trinagulo.mover(movimientoDer);
        
         
        }
         if(tecla == KeyEvent.VK_Q){
     
        CirculoGrafico bala = trinagulo.bala();
        trinagulo.balas.add(bala);
        v.add(bala);
               
         
        }
        
            
    }

    @Override
    public void keyReleased(KeyEvent ke) {
     
        int tecla = ke.getKeyCode();
        
        if(tecla == KeyEvent.VK_LEFT){
  
            this.trinagulo.mover(movimientoNulo);
          
        }
         if(tecla == KeyEvent.VK_RIGHT){
              
              this.trinagulo.mover(movimientoNulo);
           
         
        }
    }
    
    
    public void refNave(TrianguloGrafico t){
        this.trinagulo = t;
        
    }
    
    public void refAst(RectanguloGrafico b,RectanguloGrafico c,RectanguloGrafico d,RectanguloGrafico e,RectanguloGrafico f){
       
        ast.add(b);
        ast.add(c);
        ast.add(d);
        ast.add(e);
        ast.add(f);
        
    }
    public void refPuntos(TextoGrafico a)
    {
        this.puntos = a;
    }
     public void refVidas(TextoGrafico b)
    {
        this.vidas =b;
    }
      public void refFinal(TextoGrafico c)
    {
        this.finaltr = c;
    }
      
      
    public void colision(){
        
        int i,j;
        for ( i = 0; i < trinagulo.balas.size(); i++) {
         CirculoGrafico bala = (CirculoGrafico) trinagulo.balas.get(i);
            for ( j = 0; j < ast.size(); j++) {
                RectanguloGrafico aste = (RectanguloGrafico) ast.get(j);
                
                Coordenada corBala = new Coordenada(bala.getX(),bala.geyY());
                
                Coordenada derecha = new Coordenada(aste.getX()+30,aste.geyY());
                Coordenada izquierda = new Coordenada(aste.getX()-15,aste.geyY());
                Coordenada medio = new Coordenada(aste.getX(),aste.geyY());
                
                if(corBala.getX() > izquierda.getX() &&  corBala.getX() <  derecha.getX() && 
                        corBala.geyY() < medio.geyY() && corBala.geyY()+25 > medio.geyY()){
                    aste.pintar(Color.WHITE);
                    bala.pintar(Color.WHITE);
                    aste.setY(-100);
                    bala.setY(-100);
                    trinagulo.balas.remove(bala);
                    ast.remove(aste);
                    contadorAsteroides--;
                    score += 5;
                    puntos.setColor(Color.WHITE);
                    String nuevoScore = ""+score;
                    TextoGrafico newScore = new TextoGrafico(nuevoScore, Color.RED, 1700, 350);
                    newScore.setSize(40);
                    puntos = newScore;
                    v.add(puntos);
                }
                
                if(medio.geyY() > 475 && medio.geyY() < 550 && (trinagulo.cor1.getX() < medio.getX()) && (trinagulo.cor2.getX() > medio.getX())){
                    score = score - 5;
                    numVidas--;
                    String nuevoScore = ""+score;
                    String nuevaVida = ""+numVidas;
                    vidas.setColor(Color.WHITE);
                    puntos.setColor(Color.WHITE);
                    TextoGrafico nuevoNumVidas = new TextoGrafico(nuevaVida,Color.RED,0x6a4,150);
                    nuevoNumVidas.setSize(40);
                       vidas =nuevoNumVidas;
                    TextoGrafico nuevoNumScore = new TextoGrafico(nuevoScore,Color.RED,1700,350);
                    nuevoNumScore.setSize(40);
                    puntos = nuevoNumScore;
                    v.add(vidas);
                    v.add(puntos);
                    
                    ast.remove(aste);
                    aste.setY(2000);
                    contadorAsteroides--;
                    
                    
                }
            }
        }
        
    }
    public void iniciar(){
        while(FinJuego){
            
            try{
                if(!trinagulo.balas.isEmpty()){
                    trinagulo.ciclo();
                }
                
                int i;
                for (i = 0; i < ast.size(); i++) {
                    RectanguloGrafico rect = (RectanguloGrafico) ast.get(i);
                    rect.ciclo(MOV);
                    if(rect.geyY() > 525){
                    int rango = aleatorio(800,50);
                    rect.setY(0);
                    rect.setX(rango);
                }
                }
                if(contadorAsteroides < MAXAST){
                    int rango = aleatorio(800,50);
                    Coordenada inicio = new Coordenada(rango,0);
                    RectanguloGrafico nuevo = new RectanguloGrafico(inicio, 25,25,Color.GRAY);
                    ast.add(nuevo);
                    v.add(nuevo);
                    nuevo.ciclo(MOV);
                    contadorAsteroides++;
                }
                
                int nivel = 1;
                String niveles = ""+ nivel;
                TextoGrafico nivelesTexto = new TextoGrafico("NIVEL",Color.BLACK,1700,500);
                nivelesTexto.setSize(50);
                TextoGrafico numeroNivelTexto = new TextoGrafico(niveles,Color.BLUE,1700,600);
                numeroNivelTexto.setSize(40);
                v.add(nivelesTexto);
                v.add(numeroNivelTexto);
                
                
                if(score >= 10 && score < 30){
                    nivel =2;
                    String nuevoNivel =""+nivel;
                    numeroNivelTexto.setColor(Color.WHITE);
                    TextoGrafico nuevoNivelTexto = new TextoGrafico(nuevoNivel,Color.BLUE,1700,600);
                    nuevoNivelTexto.setSize(40);
                    v.add(nuevoNivelTexto);
                    MOV =12;
                    MAXAST =6;
                       for (i = 0; i < ast.size(); i++) {
                    RectanguloGrafico rect = (RectanguloGrafico) ast.get(i);
                   rect.pintar(Color.yellow);
                       }
                }
                 if(score >= 30 && score < 150){
                    nivel =3;
                    String nuevoNivel =""+nivel;
                    numeroNivelTexto.setColor(Color.WHITE);
                    TextoGrafico nuevoNivelTexto = new TextoGrafico(nuevoNivel,Color.BLUE,1700,600);
                    nuevoNivelTexto.setSize(40);
                    v.add(nuevoNivelTexto);
                    MOV =15;
                    MAXAST =7;
                       for (i = 0; i < ast.size(); i++) {
                    RectanguloGrafico rect = (RectanguloGrafico) ast.get(i);
                   rect.pintar(Color.PINK);
                       }
                }
                if(numVidas == 0){
                    FinJuego = false;
                    v.add(finaltr);
                }
                
                colision();
              
                
               Thread.sleep(70);
            } catch (InterruptedException ex) { 
                System.out.println(ex);
            }
           
            repaint();
        }
    }
    
    
}
