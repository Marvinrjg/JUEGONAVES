
package Modelo;

public class Rectangulo extends Coordenada{
    
    private float lado1;
    private float lado2;
    
    public Rectangulo(){
        super();
         this.lado1=0;
         this.lado2=0;
    }
    public Rectangulo(Coordenada coor, float x, float y){
        super(coor);
        this.lado1=x;
        this.lado2=y;
    }
    public Rectangulo(Rectangulo nuevo){
        super(nuevo.getX(), nuevo.geyY());
        this.lado1= nuevo.lado1;
        this.lado2 = nuevo.lado2;
    }
        public float getLado(int lado){
            if(lado == 1){
                return this.lado1;
            }
            if(lado==2){
                return this.lado2;
            }
            if(lado != 1 && lado != 2){
                return 0;
            }
            return 0;
        }
    
}
