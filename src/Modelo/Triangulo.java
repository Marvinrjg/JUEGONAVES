
package Modelo;

import java.util.ArrayList;

public class Triangulo extends Coordenada {
    
    public Coordenada cor1 = new Coordenada(); //izquierda
    public Coordenada cor2 = new Coordenada(); //derechqa
    
    
    ArrayList balas = new ArrayList();
    
    public Triangulo(){
        super();
        
        this.cor1.setX(0);
        this.cor1.setY(0);
        
        this.cor2.setX(0);
        this.cor2.setY(0);
        
    }
    
    public Triangulo(Coordenada a,Coordenada b,Coordenada c){
        super(a.getX(),a.geyY());
        
        this.cor1.setX(b.getX());
        this.cor1.setY(b.geyY());
        
        this.cor2.setX(c.getX());
        this.cor2.setY(c.geyY());
    }
    
    public Triangulo(Triangulo nuevoT)
    {
        super(nuevoT.getX(),nuevoT.geyY());
        
        this.cor1.setX(nuevoT.getX());
        this.cor1.setY(nuevoT.geyY());
        
        this.cor2.setX(nuevoT.getX());
        this.cor2.setY(nuevoT.geyY());
        
        
    }
    
    public void setVertice(Coordenada nva, int lado){
        if(lado == 1){
            this.setX(nva.getX());
            this.setY(nva.geyY());
        }
        if(lado == 2){
             this.cor1.setX(nva.getX());
            this.cor1.setY(nva.geyY());
        }
        if(lado == 3){
             this.cor2.setX(nva.getX());
            this.cor2.setY(nva.geyY());
        }
    }
    
    public void mover(Coordenada nuevaC){
        setVertice(this.suma(nuevaC),1);
        setVertice(this.cor1.suma(nuevaC),2);
        setVertice(this.cor2.suma(nuevaC),3);
        
        
    }
    
    
}
