
package Modelo;

import java.awt.Color;
import java.awt.Graphics;

public class CirculoGrafico extends Circulo implements Dibujable {

        Color color;
        
        public CirculoGrafico(Coordenada coor, float radio, Color uncolor){
            super(coor, radio);
            this.color = uncolor;
        }
    
    @Override
    public void dibujar(Graphics ag) {
             ag.setColor(color);
     ag.fillOval((int)(this.getX()-this.getRadio()), (int)(this.geyY() - this.getRadio()),(int)(2*this.getRadio()) , (int)(2*this.getRadio()));

    }
    public void pintar (Color uncolor){
        this.color = uncolor;
    }
}
