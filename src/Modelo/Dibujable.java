
package Modelo;

import java.awt.Graphics;

public interface Dibujable {
 
    public void dibujar(Graphics ag);
}
