
package Modelo;

public class Circulo extends Coordenada {
    
    private float radio;
    
    public Circulo(){
        super();
        this.radio =0;
    }
    
    public Circulo(Coordenada nuevaC, float r){
        super(nuevaC);
        this.radio =r;
    }
    public Circulo(Circulo cir){
        super(cir);
        this.radio = cir.radio;
    }
    
    public float getRadio(){
        return this.radio;
    }
    
    public void setRadio(float r){
        this.radio =r;
    }
    public Coordenada getCoordenada(){
        Coordenada nuevaCC = new Coordenada(this.getX(),this.geyY());
        return nuevaCC;
    }
}
